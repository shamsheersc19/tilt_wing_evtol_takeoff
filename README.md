# tilt_wing_evtol_takeoff

The scripts provided in this repository can be used to reproduce the results presented in the "Tilt-wing eVTOL takeoff trajectory optimization" manuscript submitted to the Journal of Aircraft on February 14, 2019.

These are Python scripts that require the OpenMDAO framework and the SNOPT optimizer.
If a license for SNOPT is not available, other free optimizers can be used.
However, better initial guesses and experimenting with scaling may be necessary.
See http://openmdao.org/twodocs/versions/2.6.0/index.html for more on OpenMDAO.

The results shown in Figs. 8 and 9 in the manuscript can be generated as follows using a Linux-based OS (or equivalent):
* Run the batch file using the command `bash batch` in the terminal. This will call the python scripts, run the 12 optimization cases, and move the files with the results to separate directories. 
* Then run the plotting files using the commands `python make_ns_plots.py` and `python make_s_plots.py` to create pdf files with plots of the results.

To run the optimization cases individually without using the bash script, the takeoff_cs_run_script.py can be called, for example, as `python takeoff_cs_run_script.py 100 ns`.
The first argument (e.g., `100`) is the user-specified induced-velocity factor in percent.
The second argument (e.g., `ns`) is the stall option.
Using `ns` as the stall option means that stall constraints are used and stall is not allowed.
Using `s` as the stall option means that stall is allowed.
Other user-specified inputs can be modified in the takeoff_cs_run_script.py file.

To generate the results shown in Figs. 5 and 6, comment out the acceleration constraint line in takeoff_cs_run_script.py.

Contact Shamsheer S. Chauhan (sschau@umich.edu) with any questions.