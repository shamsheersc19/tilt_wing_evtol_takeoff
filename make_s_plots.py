# --------------------------------------------------------------------------------------------------
# This contains a plotting script (should work with matplotlib versions 2.2.0+)
# This also requires a LaTeX installation, but can be used without LaTeX by modifying this script.
# Author: Shamsheer Chauhan
# --------------------------------------------------------------------------------------------------

from __future__ import division, print_function
import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['text.usetex'] = 'true'
matplotlib.rcParams['font.size'] = '20'
matplotlib.rcParams['font.weight'] = 'bold'

# plt.rc('text.latex', preamble=r'\usepackage{newtxmath} \usepackage{newtxtext}')
plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amstext}')

v2_blue = '#1f77b4'
v2_orange = '#ff7f0e'

my_blue = '#4C72B0'
my_red = '#C54E52'
my_orange = '#ff9933'
my_green = '#56A968'
my_gray = '#a9a9a9'

show_x_displs_list = True

aoa_list = []
t_list = []


fig = plt.figure(figsize=(16,21))

grid = plt.GridSpec(7,2)

ax0 = fig.add_subplot(grid[1,0])
ax1 = fig.add_subplot(grid[1,1])
ax2 = fig.add_subplot(grid[2,:])
ax3 = fig.add_subplot(grid[3,0])
ax4 = fig.add_subplot(grid[3,1])
ax5 = fig.add_subplot(grid[4,0])
ax6 = fig.add_subplot(grid[4,1])
ax7 = fig.add_subplot(grid[5,0])
ax8 = fig.add_subplot(grid[5,1])
ax9 = fig.add_subplot(grid[6,0])
ax10 = fig.add_subplot(grid[6,1])

############# kw = 0

s0_data_x = np.load("./s_0/s_0_x.npy")
s0_data_y = np.load("./s_0/s_0_y.npy")

s0_data_y_dots = np.load("./s_0/s_0_y_dots.npy")
s0_data_x_dots = np.load("./s_0/s_0_x_dots.npy")

s0_data_thetas = np.load("./s_0/s_0_thetas.npy")
s0_data_powers = np.load("./s_0/s_0_powers.npy")

s0_data_atov = np.load("./s_0/s_0_atov.npy")
s0_data_thrusts= np.load("./s_0/s_0_thrusts.npy")
s0_data_energy= np.load("./s_0/s_0_energy.npy")
s0_data_acc= np.load("./s_0/s_0_acc.npy")

s0_data_D_wings = np.load("./s_0/s_0_D_wings.npy")
s0_data_L_wings = np.load("./s_0/s_0_L_wings.npy")
s0_data_aoa = np.load("./s_0/s_0_aoa.npy")

s0_data_f_time = np.load("./s_0/s_0_ft.npy")

aoa_list.append(s0_data_aoa)
t_list.append(s0_data_f_time)


ax2.plot(s0_data_x, s0_data_y, linestyle = ':', color = my_red, label = r'$k_{\text{w}} = 0\%$'+r'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~$\kern 0.19pc {:0.1f}$'.format(round(s0_data_energy[-1].real/(3600),1)))

ax6.plot(np.linspace(0,1,len(s0_data_x_dots)) * s0_data_f_time, np.sqrt(s0_data_x_dots**2 + s0_data_y_dots**2), linestyle = ':', color = my_red)

ax3.plot(np.linspace(0,1,len(s0_data_y)) * s0_data_f_time, s0_data_y, linestyle = ':', color = my_red)

ax4.plot(np.linspace(0,1,len(s0_data_x)) * s0_data_f_time, s0_data_x, linestyle = ':', color = my_red)

ax0.plot(np.linspace(0,1,len(s0_data_thetas)) * s0_data_f_time, - s0_data_thetas / np.pi * 180 * (-1), linestyle = ':', color = my_red)

ax1.plot(np.linspace(0,1,len(s0_data_powers)) * s0_data_f_time, s0_data_powers/1e3, linestyle = ':', color = my_red)

ax5.plot(np.linspace(0,1,len(s0_data_aoa)+1)[1:] * s0_data_f_time, s0_data_aoa / np.pi * 180, linestyle = ':', color = my_red)

ax8.plot(np.linspace(0,1,len(s0_data_thrusts))[:-1] * s0_data_f_time, (s0_data_thrusts[:-1])/1e3, linestyle = ':', color = my_red)

ax7.plot(np.linspace(0,1,len(s0_data_L_wings)) * s0_data_f_time, (s0_data_L_wings) / 1000, linestyle = ':', color = my_red)

ax10.plot(np.linspace(0,1,len(s0_data_acc)) * s0_data_f_time, (s0_data_acc), linestyle = ':', color = my_red)

ax9.plot(np.linspace(0,1,len(s0_data_D_wings)) * s0_data_f_time, (s0_data_D_wings) / 1000, linestyle = ':', color = my_red)


############# kw = 25

s25_data_x = np.load("./s_25/s_25_x.npy")
s25_data_y = np.load("./s_25/s_25_y.npy")

s25_data_y_dots = np.load("./s_25/s_25_y_dots.npy")
s25_data_x_dots = np.load("./s_25/s_25_x_dots.npy")

s25_data_thetas = np.load("./s_25/s_25_thetas.npy")
s25_data_powers = np.load("./s_25/s_25_powers.npy")

s25_data_atov = np.load("./s_25/s_25_atov.npy")
s25_data_thrusts= np.load("./s_25/s_25_thrusts.npy")
s25_data_energy= np.load("./s_25/s_25_energy.npy")
s25_data_acc= np.load("./s_25/s_25_acc.npy")

s25_data_D_wings = np.load("./s_25/s_25_D_wings.npy")
s25_data_L_wings = np.load("./s_25/s_25_L_wings.npy")
s25_data_aoa = np.load("./s_25/s_25_aoa.npy")

s25_data_f_time = np.load("./s_25/s_25_ft.npy")
aoa_list.append(s25_data_aoa)
t_list.append(s25_data_f_time)


ax2.plot(s25_data_x, s25_data_y, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray, label = r'$k_{\text{w}} = 25\%$'+r'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\,${:0.1f}$'.format(round(s25_data_energy[-1].real/(3600),1)))

ax6.plot(np.linspace(0,1,len(s25_data_x_dots)) * s25_data_f_time, np.sqrt(s25_data_x_dots**2 + s25_data_y_dots**2), linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax3.plot(np.linspace(0,1,len(s25_data_y)) * s25_data_f_time, s25_data_y, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax4.plot(np.linspace(0,1,len(s25_data_x)) * s25_data_f_time, s25_data_x, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax0.plot(np.linspace(0,1,len(s25_data_thetas)) * s25_data_f_time, - s25_data_thetas / np.pi * 180 * (-1), linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax1.plot(np.linspace(0,1,len(s25_data_powers)) * s25_data_f_time, s25_data_powers/1e3, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax5.plot(np.linspace(0,1,len(s25_data_aoa)+1)[1:] * s25_data_f_time, s25_data_aoa / np.pi * 180, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax8.plot(np.linspace(0,1,len(s25_data_thrusts))[:-1] * s25_data_f_time, (s25_data_thrusts[:-1])/1e3, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax7.plot(np.linspace(0,1,len(s25_data_L_wings)) * s25_data_f_time, (s25_data_L_wings) / 1000, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax10.plot(np.linspace(0,1,len(s25_data_acc)) * s25_data_f_time, (s25_data_acc), linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)

ax9.plot(np.linspace(0,1,len(s25_data_D_wings)) * s25_data_f_time, (s25_data_D_wings) / 1000, linestyle = (0, (3, 2, 1, 2, 1, 2)), color = my_gray)


############# kw = 50

s50_data_x = np.load("./s_50/s_50_x.npy")
s50_data_y = np.load("./s_50/s_50_y.npy")

s50_data_y_dots = np.load("./s_50/s_50_y_dots.npy")
s50_data_x_dots = np.load("./s_50/s_50_x_dots.npy")

s50_data_thetas = np.load("./s_50/s_50_thetas.npy")
s50_data_powers = np.load("./s_50/s_50_powers.npy")

s50_data_atov = np.load("./s_50/s_50_atov.npy")
s50_data_thrusts= np.load("./s_50/s_50_thrusts.npy")
s50_data_energy= np.load("./s_50/s_50_energy.npy")
s50_data_acc= np.load("./s_50/s_50_acc.npy")

s50_data_D_wings = np.load("./s_50/s_50_D_wings.npy")
s50_data_L_wings = np.load("./s_50/s_50_L_wings.npy")
s50_data_aoa = np.load("./s_50/s_50_aoa.npy")

s50_data_f_time = np.load("./s_50/s_50_ft.npy")
aoa_list.append(s50_data_aoa)
t_list.append(s50_data_f_time)


ax2.plot(s50_data_x, s50_data_y, linestyle = '--', color = my_green, label = r'$k_{\text{w}} = 50\%$'+'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\,${:0.1f}$'.format(round(s50_data_energy[-1].real/(3600),1)))

ax6.plot(np.linspace(0,1,len(s50_data_x_dots)) * s50_data_f_time, np.sqrt(s50_data_x_dots**2 + s50_data_y_dots**2), linestyle = '--', color = my_green)

ax3.plot(np.linspace(0,1,len(s50_data_y)) * s50_data_f_time, s50_data_y, linestyle = '--', color = my_green)

ax4.plot(np.linspace(0,1,len(s50_data_x)) * s50_data_f_time, s50_data_x, linestyle = '--', color = my_green)

ax0.plot(np.linspace(0,1,len(s50_data_thetas)) * s50_data_f_time, - s50_data_thetas / np.pi * 180 * (-1), linestyle = '--', color = my_green)

ax1.plot(np.linspace(0,1,len(s50_data_powers)) * s50_data_f_time, s50_data_powers/1e3, linestyle = '--', color = my_green)

ax5.plot(np.linspace(0,1,len(s50_data_aoa)+1)[1:] * s50_data_f_time, s50_data_aoa / np.pi * 180, linestyle = '--', color = my_green)

ax8.plot(np.linspace(0,1,len(s50_data_thrusts))[:-1] * s50_data_f_time, (s50_data_thrusts[:-1])/1e3, linestyle = '--', color = my_green)

ax7.plot(np.linspace(0,1,len(s50_data_L_wings)) * s50_data_f_time, (s50_data_L_wings) / 1000, linestyle = '--', color = my_green)

ax10.plot(np.linspace(0,1,len(s50_data_acc)) * s50_data_f_time, (s50_data_acc), linestyle = '--', color = my_green)

ax9.plot(np.linspace(0,1,len(s50_data_D_wings)) * s50_data_f_time, (s50_data_D_wings) / 1000, linestyle = '--', color = my_green)


############# kw = 75

s75_data_x = np.load("./s_75/s_75_x.npy")
s75_data_y = np.load("./s_75/s_75_y.npy")

s75_data_y_dots = np.load("./s_75/s_75_y_dots.npy")
s75_data_x_dots = np.load("./s_75/s_75_x_dots.npy")

s75_data_thetas = np.load("./s_75/s_75_thetas.npy")
s75_data_powers = np.load("./s_75/s_75_powers.npy")

s75_data_atov = np.load("./s_75/s_75_atov.npy")
s75_data_thrusts= np.load("./s_75/s_75_thrusts.npy")
s75_data_energy= np.load("./s_75/s_75_energy.npy")
s75_data_acc= np.load("./s_75/s_75_acc.npy")

s75_data_D_wings = np.load("./s_75/s_75_D_wings.npy")
s75_data_L_wings = np.load("./s_75/s_75_L_wings.npy")
s75_data_aoa = np.load("./s_75/s_75_aoa.npy")

s75_data_f_time = np.load("./s_75/s_75_ft.npy")
aoa_list.append(s75_data_aoa)
t_list.append(s75_data_f_time)


ax2.plot(s75_data_x, s75_data_y, dashes=[5, 3, 9, 3], color = my_blue, label = r'$k_{\text{w}} = 75\%$'+'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\,${:0.1f}$'.format(round(s75_data_energy[-1].real/(3600),1)))

ax6.plot(np.linspace(0,1,len(s75_data_x_dots)) * s75_data_f_time, np.sqrt(s75_data_x_dots**2 + s75_data_y_dots**2), dashes=[5, 3, 9, 3], color = my_blue)

ax3.plot(np.linspace(0,1,len(s75_data_y)) * s75_data_f_time, s75_data_y, dashes=[5, 3, 9, 3], color = my_blue)

ax4.plot(np.linspace(0,1,len(s75_data_x)) * s75_data_f_time, s75_data_x, dashes=[5, 3, 9, 3], color = my_blue)

ax0.plot(np.linspace(0,1,len(s75_data_thetas)) * s75_data_f_time, - s75_data_thetas / np.pi * 180 * (-1), dashes=[5, 3, 9, 3], color = my_blue)

ax1.plot(np.linspace(0,1,len(s75_data_powers)) * s75_data_f_time, s75_data_powers/1e3, dashes=[5, 3, 9, 3], color = my_blue)

ax5.plot(np.linspace(0,1,len(s75_data_aoa)+1)[1:] * s75_data_f_time, s75_data_aoa / np.pi * 180, dashes=[5, 3, 9, 3], color = my_blue)

ax8.plot(np.linspace(0,1,len(s75_data_thrusts))[:-1] * s75_data_f_time, (s75_data_thrusts[:-1])/1e3, dashes=[5, 3, 9, 3], color = my_blue)

ax7.plot(np.linspace(0,1,len(s75_data_L_wings)) * s75_data_f_time, (s75_data_L_wings) / 1000, dashes=[5, 3, 9, 3], color = my_blue)

ax10.plot(np.linspace(0,1,len(s75_data_acc)) * s75_data_f_time, (s75_data_acc), dashes=[5, 3, 9, 3], color = my_blue)

ax9.plot(np.linspace(0,1,len(s75_data_D_wings)) * s75_data_f_time, (s75_data_D_wings) / 1000, dashes=[5, 3, 9, 3], color = my_blue)


############# kw = 100%

s100_data_x = np.load("./s_100/s_100_x.npy")
s100_data_y = np.load("./s_100/s_100_y.npy")

s100_data_y_dots = np.load("./s_100/s_100_y_dots.npy")
s100_data_x_dots = np.load("./s_100/s_100_x_dots.npy")

s100_data_thetas = np.load("./s_100/s_100_thetas.npy")
s100_data_powers = np.load("./s_100/s_100_powers.npy")

s100_data_atov = np.load("./s_100/s_100_atov.npy")
s100_data_thrusts= np.load("./s_100/s_100_thrusts.npy")
s100_data_energy= np.load("./s_100/s_100_energy.npy")
s100_data_acc= np.load("./s_100/s_100_acc.npy")

s100_data_D_wings = np.load("./s_100/s_100_D_wings.npy")
s100_data_L_wings = np.load("./s_100/s_100_L_wings.npy")
s100_data_aoa = np.load("./s_100/s_100_aoa.npy")

s100_data_f_time = np.load("./s_100/s_100_ft.npy")
aoa_list.append(s100_data_aoa)
t_list.append(s100_data_f_time)


ax2.plot(s100_data_x, s100_data_y, color = 'k', label = r'$k_{\text{w}} = 100\%$'+'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~$\kern -0.08pc {:0.1f}$'.format(round(s100_data_energy[-1].real/(3600),1)))

ax6.plot(np.linspace(0,1,len(s100_data_x_dots)) * s100_data_f_time, np.sqrt(s100_data_x_dots**2 + s100_data_y_dots**2), color = 'k')

ax3.plot(np.linspace(0,1,len(s100_data_y)) * s100_data_f_time, s100_data_y, color = 'k')

ax4.plot(np.linspace(0,1,len(s100_data_x)) * s100_data_f_time, s100_data_x, color = 'k')

ax0.plot(np.linspace(0,1,len(s100_data_thetas)) * s100_data_f_time, - s100_data_thetas / np.pi * 180 * (-1), color = 'k')

ax1.plot(np.linspace(0,1,len(s100_data_powers)) * s100_data_f_time, s100_data_powers/1e3, color = 'k')

ax5.plot(np.linspace(0,1,len(s100_data_aoa)+1)[1:] * s100_data_f_time, s100_data_aoa / np.pi * 180, color = 'k')

ax8.plot(np.linspace(0,1,len(s100_data_thrusts))[:-1] * s100_data_f_time, (s100_data_thrusts[:-1])/1e3, color = 'k')

ax7.plot(np.linspace(0,1,len(s100_data_L_wings)) * s100_data_f_time, (s100_data_L_wings) / 1000, color = 'k')

ax10.plot(np.linspace(0,1,len(s100_data_acc)) * s100_data_f_time, (s100_data_acc), color = 'k')

ax9.plot(np.linspace(0,1,len(s100_data_D_wings)) * s100_data_f_time, (s100_data_D_wings) / 1000, color = 'k')

############# kw = 200%

s200_data_x = np.load("./s_200/s_200_x.npy")
s200_data_y = np.load("./s_200/s_200_y.npy")

s200_data_y_dots = np.load("./s_200/s_200_y_dots.npy")
s200_data_x_dots = np.load("./s_200/s_200_x_dots.npy")

s200_data_thetas = np.load("./s_200/s_200_thetas.npy")
s200_data_powers = np.load("./s_200/s_200_powers.npy")

s200_data_atov = np.load("./s_200/s_200_atov.npy")
s200_data_thrusts= np.load("./s_200/s_200_thrusts.npy")
s200_data_energy= np.load("./s_200/s_200_energy.npy")
s200_data_acc= np.load("./s_200/s_200_acc.npy")

s200_data_D_wings = np.load("./s_200/s_200_D_wings.npy")
s200_data_L_wings = np.load("./s_200/s_200_L_wings.npy")
s200_data_aoa = np.load("./s_200/s_200_aoa.npy")

s200_data_f_time = np.load("./s_200/s_200_ft.npy")
aoa_list.append(s200_data_aoa)
t_list.append(s200_data_f_time)


ax2.plot(s200_data_x, s200_data_y, linestyle = '-.', color = my_orange, label = r'$k_{\text{w}} = 200\%$'+'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~$\kern -0.08pc {:0.1f}$'.format(round(s200_data_energy[-1].real/(3600),1)))

ax6.plot(np.linspace(0,1,len(s200_data_x_dots)) * s200_data_f_time, np.sqrt(s200_data_x_dots**2 + s200_data_y_dots**2), linestyle = '-.', color = my_orange)

ax3.plot(np.linspace(0,1,len(s200_data_y)) * s200_data_f_time, s200_data_y, linestyle = '-.', color = my_orange)

ax4.plot(np.linspace(0,1,len(s200_data_x)) * s200_data_f_time, s200_data_x, linestyle = '-.', color = my_orange)

ax0.plot(np.linspace(0,1,len(s200_data_thetas)) * s200_data_f_time, - s200_data_thetas / np.pi * 180 * (-1), linestyle = '-.', color = my_orange)

ax1.plot(np.linspace(0,1,len(s200_data_powers)) * s200_data_f_time, s200_data_powers/1e3, linestyle = '-.', color = my_orange)

ax5.plot(np.linspace(0,1,len(s200_data_aoa)+1)[1:] * s200_data_f_time, s200_data_aoa / np.pi * 180, linestyle = '-.', color = my_orange)

ax8.plot(np.linspace(0,1,len(s200_data_thrusts))[:-1] * s200_data_f_time, (s200_data_thrusts[:-1])/1e3, linestyle = '-.', color = my_orange)

ax7.plot(np.linspace(0,1,len(s200_data_L_wings)) * s200_data_f_time, (s200_data_L_wings) / 1000, linestyle = '-.', color = my_orange)

ax10.plot(np.linspace(0,1,len(s200_data_acc)) * s200_data_f_time, (s200_data_acc), linestyle = '-.', color = my_orange)

ax9.plot(np.linspace(0,1,len(s200_data_D_wings)) * s200_data_f_time, (s200_data_D_wings) / 1000, linestyle = '-.', color = my_orange)


###################### COMMON SETTINGS

for ax in fig.axes:
    matplotlib.pyplot.sca(ax)
    plt.ylabel(' ', rotation=0, labelpad=80)

ax0.set(xlabel = 'Time [s]')
ax0.set(ylabel = 'Wing angle\n' r'to vertical, $\theta$ [$^{\circ}$]')
ax0.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax0.set_ylim(ymin=0)
ax0.yaxis.set_ticks([0, 30, 60, 90])
ax0.spines['left'].set_bounds(0, 90)
ax0.spines['right'].set_visible(False)
ax0.spines['top'].set_visible(False)
ax0.plot([0, max(30, np.max(np.array(t_list)))], [90,90], linestyle = '-', linewidth = 1, color = 'k', alpha = 0.3)
ax0.text(5, 92, r'Horizontal', fontsize=16, alpha = 0.5)

ax1.set(xlabel = 'Time [s]')
ax1.set(ylabel = 'Electrical\npower [kW]')
ax1.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax1.set_ylim(0, 400)
ax1.yaxis.set_ticks([0, 100, 200, 300, 400])
ax1.spines['left'].set_bounds(0, 400)
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.plot([0, max(30, np.max(np.array(t_list)))], [311,311], linestyle = '-', linewidth = 1, color = 'k', alpha = 0.3)
ax1.text(3, 320, r'311 kW ($P_{\text{max}}$)', fontsize=16, alpha = 0.5)

ax2.set(xlabel = 'Horizontal displacement [m]')
ax2.set(ylabel = 'Vertical\ndisplacement [m]')
ax2.set_xlim(xmin=0)
ax2.set_ylim(ymin=0)
ax2.yaxis.set_ticks([0, 100, 200, 300])
ax2.xaxis.set_ticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900])
ax2.spines['left'].set_bounds(0, 300)
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.legend(bbox_to_anchor=(0.26, 4.2), frameon=False)

ax3.set(xlabel = 'Time [s]')
ax3.set(ylabel = 'Vertical\ndisplacement [m]')
ax3.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax3.yaxis.set_ticks([0, 200, 400])
ax3.spines['left'].set_bounds(0, 400)
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)

ax4.set(xlabel = 'Time [s]')
ax4.set(ylabel = 'Horizontal\ndisplacement [m]')
ax4.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax4.set_ylim(ymax=920)
ax4.yaxis.set_ticks([0, 300, 600, 900])
ax4.spines['left'].set_bounds(0, 900)
ax4.spines['right'].set_visible(False)
ax4.spines['top'].set_visible(False)

ax5.set(xlabel = 'Time [s]')
ax5.set(ylabel = 'Wing angle\n' r'of attack, $\alpha_{\text{EFS}}$ [$^{\circ}$]')
ax5.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax5.set_ylim(ymin = -5, ymax = 30)
ax5.yaxis.set_ticks([0, 15, 30])
ax5.spines['left'].set_bounds(0, 30)
ax5.spines['right'].set_visible(False)
ax5.spines['top'].set_visible(False)

ax6.set(xlabel = 'Time [s]')
ax6.set(ylabel = 'Speed,\n' r'$\sqrt{v_{\text{x}}^2 + v_{\text{y}}^2}$' ' [m/s]')
ax6.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax6.set_ylim(ymin=0)
ax6.yaxis.set_ticks([0, 25, 50, 75])
ax6.spines['left'].set_bounds(0, 75)
ax6.spines['right'].set_visible(False)
ax6.spines['top'].set_visible(False)

ax7.set(xlabel = 'Time [s]')
ax7.set(ylabel = r'$L_{\text{wing}}$ [kN]')
ax7.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax7.set_ylim(-0.3,10)
ax7.yaxis.set_ticks([0, 5, 10, 10])
ax7.spines['left'].set_bounds(0, 10)
ax7.spines['right'].set_visible(False)
ax7.spines['top'].set_visible(False)

ax8.set(xlabel = 'Time [s]')
ax8.set(ylabel = 'Total thrust [kN]')
ax8.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax8.set_ylim(0,10)
ax8.yaxis.set_ticks([0, 5, 10])
ax8.spines['left'].set_bounds(0, 10)
ax8.spines['right'].set_visible(False)
ax8.spines['top'].set_visible(False)

ax9.set(xlabel = 'Time [s]')
ax9.set(ylabel = r'$D_{\text{wing}}$ [kN]')
ax9.set_xlim(xmin=0, xmax = max(20, np.max(np.array(t_list))))
ax9.set_ylim(-0.05,1)
ax9.yaxis.set_ticks([0., 0.5, 1.0])
ax9.spines['left'].set_bounds(0, 1)
ax9.spines['right'].set_visible(False)
ax9.spines['top'].set_visible(False)

ax10.set(xlabel = 'Time [s]')
ax10.set(ylabel = "Acceleration\nmagnitude [$g$'s]")
ax10.set_xlim(xmin=0, xmax = max(30, np.max(np.array(t_list))))
ax10.set_ylim(ymin = 0, ymax = 0.3)
if max(s100_data_acc) > 0.4:
    ax10.yaxis.set_ticks([0., 0.3, 0.6, 0.9, 1.2])
    ax10.spines['left'].set_bounds(0, 1.2)
else:
    ax10.yaxis.set_ticks([0., 0.1, 0.2, 0.3])
    ax10.spines['left'].set_bounds(0, 0.3)
ax10.spines['right'].set_visible(False)
ax10.spines['top'].set_visible(False)


ax5.plot([0, max(30, np.max(np.array(t_list)))], [15,15], linestyle = '-', linewidth = 1, color = 'k', alpha = 0.3)
ax5.text(13, 17, r'15$^\circ\,$(stall angle)', fontsize=16, alpha = 0.5)
ax0.annotate(r'Propeller-induced velocity factors~~~~~~~~~~~~~~Energy consumed [Wh]',
            xy=(.03, .985), xycoords='figure fraction')

if show_x_displs_list:
    ax2.text(500, 1000+300, 'x displacements')
    ax2.text(500, 1000+200, str(round(s0_data_x[-1].real,1)))
    ax2.text(500, 950+200, str(round(s25_data_x[-1].real,1)))
    ax2.text(500, 900+200, str(round(s50_data_x[-1].real,1)))
    ax2.text(500, 850+200, str(round(s75_data_x[-1].real,1)))
    ax2.text(500, 800+200, str(round(s100_data_x[-1].real,1)))
    ax2.text(500, 750+200, str(round(s200_data_x[-1].real,1)))


fig.tight_layout()
plt.subplots_adjust(hspace=0.6, wspace=0.6)
# plt.show()
# plt.savefig('opt_s.png', dpi=300)
plt.savefig('opt_results_s.pdf')
